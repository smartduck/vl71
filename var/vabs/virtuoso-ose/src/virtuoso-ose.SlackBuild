#!/bin/sh

# Copyright 2009, 2010, 2011  Eric Hameleers, Eindhoven, NL
# Copyright 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.

# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="virtuoso"
VERSION=${VERSION:-6.1.6}
BUILD=${BUILD:-2vl71}
LINK=${LINK:-"http://downloads.sf.net/$NAME/$NAME-opensource-$VERSION.tar.gz"}
MAKEDEPENDS="slapt-get openssl curl"
# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:--j7}

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp

PKG=$TMP/package-virtuoso-ose

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf virtuoso-opensource-$VERSION
tar xf $CWD/virtuoso-opensource-$VERSION.tar.gz --checkpoint=10000 || exit 1
cd virtuoso-opensource-$VERSION || exit 1

# Apply a patch that fixes Unicode issue as described here:
#   https://bugs.kde.org/show_bug.cgi?id=271664


chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Configure:
# We disable building of all VADs because Slackware's version of Virtuoso is
# only used as backend for the Soprano/Nepomuk desktop search.
# This saves us a lot of megabytes.
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
LDFLAGS="-L/usr/lib${LIBDIRSUFFIX}" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --docdir=/usr/doc/virtuoso-ose-$VERSION \
  --with-pthreads \
  --enable-aio \
  --disable-static \
  --disable-all-vads \
  --program-prefix= \
  --program-suffix= \
  --build=$ARCH-slackware-linux

# Build and install:
make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

# Do not clobber an existing .ini file:
mkdir -p $PKG/etc
mv $PKG/var/lib/virtuoso/db/virtuoso.ini $PKG/etc/virtuoso.ini.new
ln -sf /etc/virtuoso.ini $PKG/var/lib/virtuoso/db/

find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

mkdir -p $PKG/usr/doc/virtuoso-ose-$VERSION
cp -a \
  AUTHORS COPYING CREDITS LICENSE NEWS README* \
  $PKG/usr/doc/virtuoso-ose-$VERSION

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-ose-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
