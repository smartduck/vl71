#!/bin/sh

# Copyright 2006, 2008, 2009, 2010, 2011, 2012  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## build glibc-$VERSION for Slackware
NAME=${NAME:-glibc}
VERSION=${VERSION:-2.20}
CHECKOUT=${CHECKOUT:-""}
LINK=${LINK:-"ftp://ftp.gnu.org/gnu/$NAME/$NAME-$VERSION.tar.xz"}
LINK1=${LINK1:-"http://www.iana.org/time-zones/repository/releases/tzdata2015a.tar.gz"}
LINK2=${LINK2:-"http://www.iana.org/time-zones/repository/releases/tzcode2015a.tar.gz"}
## Included in glibc now:
## glibc-libidn version
#LIBIDNVER=2.10.1

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"!gcc !kernel-headers gcc gawk kernel-headers"} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------


# $ARCH may be preset, otherwise i486 compatibility with i686 binary
# structuring is the Slackware default, since this is what gcc-3.2+
# requires for binary compatibility with previous releases.
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

# I'll break this out as an option for fun  :-)
case $ARCH  in
  i386)
    OPTIMIZ="-O3 -march=i386 -mcpu=i686"
    LIBDIRSUFFIX=""
    ;;
  i486)
    OPTIMIZ="-O3 -march=i486 -mtune=i686"
    LIBDIRSUFFIX=""
    ;;
  i586)
    OPTIMIZ="-O3 -march=i586 -mtune=i686"
    LIBDIRSUFFIX=""
    ;;
  i686)
    OPTIMIZ="-O3 -march=i686"
    LIBDIRSUFFIX=""
    ;;
  athlon)
    OPTIMIZ="-O3 -march=athlon"
    LIBDIRSUFFIX=""
    ;;
  s390)
    OPTIMIZ="-O3"
    LIBDIRSUFFIX=""
    ;;
  x86_64)
    OPTIMIZ="-O3 -fPIC"
    LIBDIRSUFFIX="64"
    ;;
  *)
    OPTIMIZ="-O3"
    LIBDIRSUFFIX=""
    ;;
esac

case $ARCH in
  x86_64)
    TARGET=${TARGET:-x86_64-vlocity-linux}
    ;;
  i586)
    # This should be i486 for all 32-bit x86 arch:
    TARGET=${TARGET:-i586-vector-linux}
    ;;
esac

# Hand off the $ARCH variable to $SLACKWARE_ARCH to avoid confusing glibc:
SLACKWARE_ARCH=$ARCH
unset ARCH

CVSVER=${VERSION}${CHECKOUT}

# NOTE!!!  glibc needs to be built against the sanitized kernel headers,
# which will be installed under /usr/include by the kernel-headers package.
# Be sure the correct version of the headers package is installed BEFORE
# building glibc!

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
# Temporary build location.  This should not be a directory
# path a non-root user could create later...
TMP=${TMP:-$RELEASEDIR/glibc-tmp-$(mcookie)}
mkdir -p $TMP

NUMJOBS=${NUMJOBS:--j4}

# Sanity check on the version number in the install scripts:
if ! grep -vq libutil-${VERSION}.so $CWD/doinst.sh-glibc ; then
  echo "FATAL:  doinst.sh scripts have wrong version numbers."
  exit 1
fi

# This function fixes a doinst.sh file for x86_64.
# With thanks to Fred Emmott.
fix_doinst() {
  if [ "x$LIBDIRSUFFIX" = "x" ]; then
    return;
  fi;
  # Fix "( cd usr/lib ;" occurrences
  sed -i "s#lib ;#lib${LIBDIRSUFFIX} ;#" install/doinst.sh
  # Fix "lib/" occurrences
  sed -i "s#lib/#lib${LIBDIRSUFFIX}/#g" install/doinst.sh
  # Fix "( cd lib" occurrences
  sed -i "s#( cd lib\$#( cd lib${LIBDIRSUFFIX}#" install/doinst.sh

  if [ "$SLACKWARE_ARCH" = "x86_64" ]; then
    sed -i 's#ld-linux.so.2#ld-linux-x86-64.so.2#' install/doinst.sh
  fi
}

# This is going to be the initial $DESTDIR:
export PKG=$TMP/package-glibc-incoming-tree
PGLIBC=$TMP/package-glibc
PSOLIBS=$TMP/package-glibc-solibs
PZONE=$TMP/package-glibc-zoneinfo
PI18N=$TMP/package-glibc-i18n
PPROFILE=$TMP/package-glibc-profile
PDEBUG=$TMP/package-glibc-debug

# Empty these locations first:
for dir in $PKG $PGLIBC $PSOLIBS $PZONE $PI18N $PPROFILE $PDEBUG ; do
  if [ -d $dir ]; then
    rm -rf $dir
  fi
  mkdir -p $dir
done
if [ -d $TMP/glibc-$VERSION ]; then
  rm -rf $TMP/glibc-$VERSION
fi

# Create an incoming directory structure for glibc to be built into:
mkdir -p $PKG/lib${LIBDIRSUFFIX}
mkdir -p $PKG/sbin
mkdir -p $PKG/usr/bin
mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}
mkdir -p $PKG/usr/sbin
mkdir -p $PKG/usr/include
mkdir -p $PKG/usr/doc
mkdir -p $PKG/usr/man
mkdir -p $PKG/usr/share
mkdir -p $PKG/var/db/nscd
mkdir -p $PKG/var/run/nscd

#get the source..........
for SRC in $(echo $LINK $LINK1 $LINK2);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done

# Begin extract/compile:
cd $TMP
rm -rf glibc-$CVSVER
tar xvf $CWD/glibc-$CVSVER.tar.xz \
  || tar xvf $CWD/glibc-$CVSVER.tar.bz2 \
  || tar xvf $CWD/glibc-$CVSVER.tar.gz
cd glibc-$CVSVER

#tar xvf $CWD/glibc-libidn-$LIBIDNVER.tar.?z*
#mv glibc-libidn-$LIBIDNVER libidn

chown -R root:root .
find . -perm 666 -exec chmod 644 {} \;
find . -perm 664 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;

# Clean up leftover CVS directories:
find . -type d -name CVS -exec rm -r {} \; 2> /dev/null

# Apply patches; exit if any fail.
#PATCHES
#-----------------------------------------------------
# Put any Patches here *NOTE this only works if all 
# your patches use the -p1 strip option!
#-----------------------------------------------------
for i in $CWD/patches/*;do
#  patch -p1 <$i
  mkdir -p $PKG/usr/doc/$NAME-$VERSION/patches/
  cp $i $PKG/usr/doc/$NAME-$VERSION/patches/
done

# APPLY PATCHES
# This is a patch function to put all glibc patches in the build script
# up near the top.
apply_patches() {
  # Use old-style locale directories rather than a single (and strangely
  # formatted) /usr/lib/locale/locale-archive file:
  zcat $CWD/patches/glibc.locale.no-archive.diff.gz | patch -p1 --verbose || return 1
  # The is_IS locale is causing a strange error about the "echn" command
  # not existing.  This patch reverts is_IS to the version shipped in
  # glibc-2.5:
  zcat $CWD/patches/is_IS.diff.gz | patch -p1 --verbose || return 1
  # Fix NIS netgroups:
  zcat $CWD/patches/glibc.nis-netgroups.diff.gz | patch -p1 --verbose || return 1
  # Support ru_RU.CP1251 locale:
  zcat $CWD/patches/glibc.ru_RU.CP1251.diff.gz | patch -p1 --verbose || return 1
  # Fix resolver problem with glibc-2.9:
  zcat $CWD/patches/glibc-2.10-dns-no-gethostbyname4.diff.gz | patch -p0 --verbose || return 1
  # This reverts a patch that was made to glibc to fix "namespace leakage",
  # which seems to cause some build failures (e.g. with conntrack):
  zcat $CWD/patches/glibc.revert.to.fix.build.breakages.diff.gz | patch -p1 -l --verbose || return 1
  # This partial security patch still applies and might be needed:
  zcat $CWD/patches/glibc.git-96611391ad8823ba58405325d78cefeae5cdf699-CVE-2010-3847b.patch.gz | patch -p1 --verbose || return 1
  # Make it harder for people to trick ldd into running code:
  zcat $CWD/patches/glibc.ldd.trace.through.dynamic.linker.diff.gz | patch -p1 --verbose || return 1
  # Make glibc compile with binutils using --enable-initfini-array.
  # At this time, we do not recommend this due to probable ABI breakage.
  # The also patch needs work before it would apply.
  # ***NOT READY***
  #zcat $CWD/glibc.git-4a531bb0b3b582cb693de9f76d2d97d970f9a5d5.patch.gz | patch -p1 --verbose || exit 1
  #
  # 2014-05:  We'll try building with the stock asm...
  ## Avoid the Intel optimized asm routines for now because they break
  ## the flash player.  We'll phase this in when it's safer to do so.
  #zcat $CWD/glibc.disable.broken.optimized.memcpy.diff.gz | patch -p1 --verbose || exit 1
  # Security hardening patch from Florian Weimer:
  patch -p1 --verbose < $CWD/patches/glibc.hardening.diff || return 1
}

apply_patches || exit 1

# END OF APPLY PATCHES

sed -i 's#<rpc/types.h>#"rpc/types.h"#' sunrpc/rpc_clntout.c
sed -i '/test-installation.pl/d' Makefile
sed -i 's|@BASH@|/bin/bash|' elf/ldd.bash.in
#ln -s /usr/bin/awk /usr/bin/nawk
#-----------------------------------------------------
if [ ! $? = 0 ]; then
  exit 1
fi


# Make build directory:
mkdir build-glibc-$VERSION
cd build-glibc-$VERSION || exit 1

echo "BUILDING DAS NPTL GLIBC"
CFLAGS="-g $OPTIMIZ" \
../configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --enable-kernel=3.2.39 \
  --enable-obsolete-rpc \
  --with-headers=/usr/include \
  --enable-add-ons \
  --enable-profile \
  --infodir=/usr/info \
  --mandir=/usr/man \
  --with-tls \
  --with-__thread \
  --without-cvs \
  --build=$TARGET \
  --host=$TARGET \
  --target=$TARGET \
  $TARGET || exit 1

make $NUMJOBS || make || exit 1
make install install_root=$PKG || exit 1
make localedata/install-locales install_root=$PKG || exit 1


# Build timezone/info data
rm -rf tzcodedata-build
mkdir tzcodedata-build
cd tzcodedata-build
tar xzf $CWD/tzdata?????.tar.gz || exit 1
tar xzf $CWD/tzcode?????.tar.gz || exit 1
sed -i "s,/usr/local,$(pwd),g" Makefile
sed -i "s,/etc/zoneinfo,/zoneinfo,g" Makefile
make || exit 
make install || exit 1
mkdir -p $PKG/usr/share/zoneinfo/{posix,right}
cd etc
cp -a zoneinfo/* $PKG/usr/share/zoneinfo
cp -a zoneinfo-posix/* $PKG/usr/share/zoneinfo/posix
cp -a zoneinfo-leaps/* $PKG/usr/share/zoneinfo/right
rm -rf $PKG/usr/share/zoneinfo/localtime


# The prevailing standard seems to be putting unstripped libraries in
# /usr/lib/debug/ and stripping the debugging symbols from all the other
# libraries.
mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}/debug
cp -a $PKG/lib${LIBDIRSUFFIX}/l*.so* $PKG/usr/lib${LIBDIRSUFFIX}/debug
cp -a $PKG/usr/lib${LIBDIRSUFFIX}/*.a $PKG/usr/lib${LIBDIRSUFFIX}/debug
# Don't need debug+profile:
( cd $PKG/usr/lib${LIBDIRSUFFIX}/debug ; rm -f *_p.* )
# NOTE:  Is there really a reason for the glibc-debug package?
# If you're debugging glibc, you can also compile it, right?

## COMMENTED OUT:  There's no reason for profile libs to include -g information.
## Put back unstripped profiling libraries:
#mv $PKG/usr/lib${LIBDIRSUFFIX}/debug/*_p.a $PKG/usr/lib${LIBDIRSUFFIX}
# It might be best to put the unstripped and profiling libraries in glibc-debug and glibc-profile.

# I don't think "strip -g" causes the pthread problems.  It's --strip-unneeded that does.
strip -g $PKG/lib${LIBDIRSUFFIX}/l*.so*
strip -g $PKG/usr/lib${LIBDIRSUFFIX}/l*.so*
strip -g $PKG/usr/lib${LIBDIRSUFFIX}/lib*.a

# Back to the sources dir to add some files/docs:
cd $TMP/glibc-$CVSVER

# We'll automatically install the config file for the Name Server Cache Daemon.
# Perhaps this should also have some commented-out startup code in rc.inet2...
mkdir -p $PKG/etc
cat nscd/nscd.conf > $PKG/etc/nscd.conf.new

# Install some scripts to help select a timezone:
mkdir -p $PKG/var/log/setup
cp -a $CWD/timezone-scripts/setup.timeconfig $PKG/var/log/setup
chown root:root $PKG/var/log/setup/setup.timeconfig
chmod 755 $PKG/var/log/setup/setup.timeconfig
mkdir -p $PKG/usr/sbin
cp -a $CWD/timezone-scripts/timeconfig $PKG/usr/sbin
chown root:root $PKG/usr/sbin/timeconfig
chmod 755 $PKG/usr/sbin/timeconfig

## Install docs:
( mkdir -p $PKG/usr/doc/glibc-$VERSION
  cp -a \
    BUGS CONFORMANCE COPYING COPYING.LIB FAQ INSTALL LICENSES NAMESPACE \
    NEWS NOTES PROJECTS README README.libm \
    $PKG/usr/doc/glibc-$VERSION
)

mkdir -p $PKG/usr/share/zoneinfo/
# Don't forget to add the /usr/share/zoneinfo/localtime -> /etc/localtime symlink! :)
if [ ! -r $PKG/usr/share/zoneinfo/localtime ]; then
  ( cd $PKG/usr/share/zoneinfo ; ln -sf /etc/localtime . )
fi

# OK, there are some very old Linux standards that say that any binaries in a /bin or
# /sbin directory (and the directories themselves) should be group bin rather than
# group root, unless a specific group is really needed for some reason.
#
# I can't find any mention of this in more recent standards docs, and always thought
# that it was pretty cosmetic anyway (hey, if there's a reason -- fill me in!), so
# it's possible that this ownership change won't be followed in the near future
# (it's a PITA, and causes many bug reports when the perms change is occasionally
# forgotten).
#
# But, it's hard to get me to break old habits, so we'll continue the tradition here:
#
# No, no we won't.  You know how we love to break traditions.

# Strip most binaries:
( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-debug 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip -g 2> /dev/null
)

# Fix info dir:
rm $PKG/usr/info/dir
gzip -9 $PKG/usr/info/*

# This is junk
rm $PKG/etc/ld.so.cache
( cd $PKG
  find . -name "*.orig" -exec rm {} \;
)

##################################
# OK, time to make some packages #
##################################

# glibc-zoneinfo.  We will start with an easy one to avoid breaking a sweat.  ;-)
cd $CWD
ZONE_VERSIONS="$(echo tzcode* | cut -f1 -d . | cut -b7-11)_$(echo tzdata* | cut -f1 -d . | cut -b7-11)"
echo $ZONE_VERSIONS

cd $PZONE
# Install some scripts to help select a timezone:
mkdir -p $PZONE/var/log/setup
cp -a $CWD/timezone-scripts/setup.timeconfig $PZONE/var/log/setup
chown root:root $PZONE/var/log/setup/setup.timeconfig
chmod 755 $PZONE/var/log/setup/setup.timeconfig
mkdir -p $PZONE/usr/sbin
cp -a $CWD/timezone-scripts/timeconfig $PZONE/usr/sbin
chown root:root $PZONE/usr/sbin/timeconfig
chmod 755 $PZONE/usr/sbin/timeconfig
mkdir $PZONE/install
cat $CWD/doinst.sh-glibc-zoneinfo > $PZONE/install/doinst.sh
cat $CWD/slack-desc.glibc-zoneinfo > $PZONE/install/slack-desc
mkdir -p $PZONE/usr/share
cd $PZONE/usr/share
cp -a --verbose $PKG/usr/share/zoneinfo . || exit 1
cd $PZONE
mkdir -p $PZONE/etc
# This is already hard-coded into doinst.sh (like it'll be there anyway ;-):
rm -f etc/localtime
# Wrap it up:
/sbin/makepkg -l y -c n $RELEASEDIR/glibc-zoneinfo-${ZONE_VERSIONS}-noarch-${BUILD}.txz || exit 1

# glibc-profile:
cd $PPROFILE
mkdir -p usr/lib${LIBDIRSUFFIX}
# Might as well just grab these with 'mv' to simplify things later:
mv $PKG/usr/lib${LIBDIRSUFFIX}/lib*_p.a usr/lib${LIBDIRSUFFIX}
# Profile libs should be stripped.  Use the debug libs to debug...
( cd usr/lib${LIBDIRSUFFIX} ; strip -g *.a )
mkdir install
cp -a $CWD/slack-desc.glibc-profile install/slack-desc
#makepkg -l y -c n $RELEASEDIR/glibc-profile-$VERSION-$SLACKWARE_ARCH-$BUILD.txz

# THIS IS NO LONGER PACKAGED (or is it?  might be better to let it be made, and then ship it or not...)
# glibc-debug:
cd $PDEBUG
mkdir -p usr/lib${LIBDIRSUFFIX}
# Might as well just grab these with 'mv' to simplify things later:
mv $PKG/usr/lib${LIBDIRSUFFIX}/debug usr/lib${LIBDIRSUFFIX}
mkdir install
cp -a $CWD/slack-desc.glibc-debug install/slack-desc
#makepkg -l y -c n $RELEASEDIR/glibc-debug-$VERSION-$SLACKWARE_ARCH-$BUILD.txz
## INSTEAD, NUKE THESE LIBS
#rm -rf $PKG/usr/lib${LIBDIRSUFFIX}/debug

# glibc-i18n:
cd $PI18N
mkdir -p usr/lib${LIBDIRSUFFIX}
rm -rf usr/lib${LIBDIRSUFFIX}/locale
cp -a $PKG/usr/lib${LIBDIRSUFFIX}/locale usr/lib${LIBDIRSUFFIX}
mkdir -p usr/share
cp -a $PKG/usr/share/i18n usr/share
cp -a $PKG/usr/share/locale usr/share
mkdir install
cp -a $CWD/slack-desc.glibc-i18n install/slack-desc
#makepkg -l y -c n $RELEASEDIR/glibc-i18n-$VERSION-$SLACKWARE_ARCH-$BUILD.txz

# glibc-solibs:
cd $PSOLIBS
mkdir -p etc/profile.d
cp -a $CWD/profile.d/* etc/profile.d
chown -R root:root etc
chmod 755 etc/profile.d/*
mkdir -p lib${LIBDIRSUFFIX}
cp -a $PKG/lib${LIBDIRSUFFIX}/* lib${LIBDIRSUFFIX}
( cd lib${LIBDIRSUFFIX}
  mkdir incoming
  mv *so* incoming
  mv incoming/libSegFault.so .
)
mkdir -p usr
cp -a $PKG/usr/bin usr
mv usr/bin/ldd .
rm usr/bin/*
mv ldd usr/bin
mkdir -p usr/lib${LIBDIRSUFFIX}
# The gconv directory has a lot of stuff, but including it here will save some problems.
# Seems standard elsewhere.
cp -a $PKG/usr/lib${LIBDIRSUFFIX}/gconv usr/lib${LIBDIRSUFFIX}
# Another manpage abandoned by GNU...
#mkdir -p usr/man/man1
#cp -a $PKG/usr/man/man1/ldd.1.gz usr/man/man1
mkdir -p usr/libexec
cp -a $PKG/usr/libexec/pt_chown usr/libexec
# Same usr.bin deal:
cp -a $PKG/sbin .
mv sbin/ldconfig .
rm sbin/*
mv ldconfig sbin
mkdir install
cp -a $CWD/slack-desc.glibc-solibs install/slack-desc
cp -a $CWD/doinst.sh-glibc-solibs install/doinst.sh
fix_doinst
# Ditch links:
find . -type l -exec rm {} \;
# Build the package:
#makepkg -l y -c n $RELEASEDIR/glibc-solibs-$VERSION-$SLACKWARE_ARCH-$BUILD.txz

# And finally, the complete "all-in-one" glibc package is created
# from whatever was leftover:
cd $PGLIBC
mv $PKG/* .
mkdir -p etc/profile.d
cp -a $CWD/profile.d/* etc/profile.d
chown -R root:root etc
chmod 755 etc/profile.d/*
# Ditch links (these are in doinst.sh-glibc):
find . -type l -exec rm {} \;
mkdir install
cp -a $CWD/slack-desc.glibc install/slack-desc
cp -a $CWD/doinst.sh-glibc install/doinst.sh
fix_doinst
( cd lib${LIBDIRSUFFIX}
  mkdir incoming
  mv *so* incoming
  mv incoming/libSegFault.so .
)
# Build the package:
/sbin/makepkg -l y -c n $RELEASEDIR/glibc-$VERSION-$SLACKWARE_ARCH-$BUILD.txz

# Done!
echo
echo "glibc packages built in $RELEASEDIR!"
rm -rf $TMP
