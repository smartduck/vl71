#!/bin/sh

# Slackware build script for grip

# Written by B. Watson (yalhcru@gmail.com)

NAME="grip"
VERSION=${VERSION:-"2.96"}
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"M0E-lnx"}   #Enter your Name!
LINK=${LINK:-"http://pkgs.fedoraproject.org/repo/pkgs/${NAME}/${NAME}+cdpar-${VERSION}.tgz/0755bfec3d6b4e4cf8f8fbd3a9d70802/${NAME}+cdpar-${VERSION}.tgz"}  #Enter URL for package here!
#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"gtk+ cdparanoia"}
#----------------------------------------------------------------------------

if [ "$NORUN" != "1" ]; then

PRGNAM=$NAME
CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
RELEASEDIR=${RELEASEDIR:-"$CWD/.."}

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
fi

set -e

for link in $(echo $LINK); do
	(
	cd $CWD
	wget -t 0 -c --no-check-certificate $link
	)
done

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
tar xvf $CWD/$(basename $LINK) || exit 1
cd $PRGNAM-$VERSION
chown -R root:root .
chmod -R a-s,u+w,go+r-w .

# Slack 12.2 and up keeps cdda headers in /usr/include/cdda, 12.1 has them
# in /usr/include itself. Be adaptable.
if [ -r /usr/include/cdda/cdda_interface.h ]; then
	SLKCFLAGS="$SLKCFLAGS -I/usr/include/cdda"
fi

# Patch to use system-installed cdparanoia libs:
patch -p1 --verbose < $CWD/system_cdparanoia_libs.diff  || exit 1

# Patch to fix a compile issue with threads (probably came from gentoo):
patch -p1 --verbose < $CWD/grip2-nptl.diff  || exit 1

# The Makefile ignores any CFLAGS we pass in, so:
perl -i.bak -pe 's,(CFLAGS\s*=\s*),$1 '"$SLKCFLAGS"' ,' Makefile || exit 1

make || exit 1

# DESTDIR not supported.
make install PREFIX=$PKG/usr || exit 1

strip --strip-unneeded $PKG/usr/bin/*

rm -f $PKG/usr/man/man1/gcd.1
gzip $PKG/usr/man/man1/$PRGNAM.1
( cd $PKG/usr/man/man1 && ln -s $PRGNAM.1.gz gcd.1.gz )

# Why does it create an empty usr/lib dir?
rm -rf $PKG/usr/lib

mkdir -p $PKG/usr/share/{applications,pixmaps}
cp $CWD/grip.desktop $PKG/usr/share/applications
cp pixmaps/grip.xpm $PKG/usr/share/pixmaps

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  $CWD/dot.grip.sample CHANGES CREDITS LICENSE README TODO \
  $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
chown -R root:root $PKG/usr/doc ; chmod 0644 $PKG/usr/doc/$PRGNAM-$VERSION/*

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh
cat $CWD/slack-desc > $RELEASEDIR/slack-desc

cd $PKG
#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"

/sbin/makepkg -l y -c n $RELEASEDIR/$PRGNAM-$VERSION-$ARCH-$BUILD.txz
fi
