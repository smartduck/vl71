#!/bin/sh

# Copyright 2011, 2012, 2013, 2014, 2015  Eric Hameleers, Eindhoven, The Netherlands
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# This script is a modified version of alien's slackbuild.  Modifications made to
# make it compatible with vlbuildbot

NAME1=openjdk
NAME2=openjre
JAVAVER=7u85
JAVABLD=b01
ICEDTEA=2.6.1

VERSION="${JAVAVER}_${JAVABLD}"
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
NUMJOBS=${NUMJOBS:--j6}
LINK=${LINK:-"http://icedtea.wildebeest.org/download/source/icedtea-$ICEDTEA.tar.xz \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/openjdk_icedtea-$ICEDTEA.tar.bz2 \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/hotspot_icedtea-$ICEDTEA.tar.bz2 \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/corba_icedtea-$ICEDTEA.tar.bz2 \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/cacao_icedtea-$ICEDTEA.tar.xz \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/jamvm_icedtea-$ICEDTEA.tar.gz \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/jaxp_icedtea-$ICEDTEA.tar.bz2 \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/jaxws_icedtea-$ICEDTEA.tar.bz2 \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/jdk_icedtea-$ICEDTEA.tar.bz2 \
 http://www.slackware.com/~alien/slackbuilds/openjdk/build/sources/langtools_icedtea-$ICEDTEA.tar.bz2 \
  "}
MAKEDEPENDS="apache-ant cpio mozilla-nss gcc-java cups"

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
TMP=$RELEASEDIR/tmp
PKG1=$TMP/package-$NAME1
PKG2=$TMP/package-$NAME2

if [ "$NORUN" != 1 ]; then

# Download the sources
mkdir -p $CWD/sources
cd $CWD/sources
for src in $LINK; do
	wget -c --no-check-certificate $src || exit 1
done
cd $CWD

# This is necessary because our gcc in 7.1 does not have java enabled.
sh $CWD/create_gcj_jvm.sh || exit 1

# If BOOTSTRAP is set to "YES" then we bootstrap the build using GCC's
# java compiler (gcj) instead of a depending on a pre-installed version
# of OpenJDK to build OpenJDK.
BOOTSTRAP=${BOOTSTRAP:-"YES"}

# If you want debug symbols in your JVM library, set DEBUGBUILD to "YES";
# this would inflate the libjvm.so file from 13 to 160 MB (estimate):
DEBUGBUILD=${DEBUGBUILD:-"NO"}

# Sources the script requires. Note that the Makefile contains SHA256SUM values
# for all of these tarballs; if the tarball does not verify then it will be
# downloaded again:

SRC_CACAO=cacao_icedtea-${ICEDTEA}.tar.gz SRC_JAMVM=jamvm_icedtea-${ICEDTEA}.tar.gz 
SRC_CORBA=corba_icedtea-${ICEDTEA}.tar.bz2 SRC_HOTSPOT=hotspot_icedtea-${ICEDTEA}.tar.bz2 
SRC_JAXP=jaxp_icedtea-${ICEDTEA}.tar.bz2 SRC_JAXWS=jaxws_icedtea-${ICEDTEA}.tar.bz2 SRC_JDK=jdk_icedtea-${ICEDTEA}.tar.bz2 
SRC_LANGTOOLS=langtools_icedtea-${ICEDTEA}.tar.bz2 SRC_OPENJDK=openjdk_icedtea-${ICEDTEA}.tar.bz2

# Automatically determine the architecture we're building on:
MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$MARCH" in
    i?86)    export ARCH=i586 ;;
    armv7hl) export ARCH=$MARCH ;;
    arm*)    export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
    *)       export ARCH=$MARCH ;;
  esac
fi

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
  LIB_ARCH=i386
  ARCH_OPTS=" --disable-jamvm "
  CONFIGURE_TRIPLET="i586-vector-linux"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  LIB_ARCH=amd64
  ARCH_OPTS=" --disable-jamvm "
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
  LIB_ARCH=$ARCH
  ARCH_OPTS=" --disable-jamvm "
fi

rm -rf $PKG1 $PKG2
mkdir -p $TMP $PKG1 $PKG2
cd $TMP
rm -rf icedtea-$ICEDTEA
tar xvf $CWD/sources/icedtea-$ICEDTEA.tar.?z || exit 1
cd icedtea-$ICEDTEA || exit 1

chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# First check: are our sources complying with our SlackBuild?
JDKU=$(cat Makefile.in | grep ^JDK_UPDATE_VERSION | tr -d ' ' | cut -f2 -d=)
JDKB=$(cat Makefile.in | grep ^BUILD_VERSION | tr -d ' ' | cut -f2 -d=)
echo "** This SlackBuild creates a package for Java $JAVAVER using OpenJDK build $JAVABLD"
echo "** The script uses icedtea-$ICEDTEA as a build framework."
echo "** Icedtea will (download and) build Java update $JDKU using OpenJDK build $JDKB"
echo "** If you see a mismatch here, then you have 5 seconds to abort this script (press Ctrl-C) and fix it."
# sleep 5
# We start with creating a proper build environment.

# Prevent an error which aborts the build:
mkdir -p bootstrap/boot/jre/lib

unset CLASSPATH
unset JAVA_HOME
unset MAKEFLAGS

JAVA_HOME=/usr/lib${LIBDIRSUFFIX}/jvm PATH=/usr/lib${LIBDIRSUFFIX}/jvm/bin:$PATH
if [ "$BOOTSTRAP" = "YES" -o "$BOOTSTRAP" = "yes" ]; then
  # Use gcc's java compiler:
  export JVMDIR=/usr/lib${LIBDIRSUFFIX}/jvm
  export ALT_BOOTDIR=/usr/lib${LIBDIRSUFFIX}/jvm
  export ECJJAR=/usr/share/java/ecj.jar
  export RTJAR=$TMP/icedtea-$ICEDTEA/bootstrap/boot/jre/lib/rt.jar
  ! echo $PATH | grep -q jvm/bin && export PATH=/usr/lib${LIBDIRSUFFIX}/jvm/bin:$PATH
  EXTRA_OPTS="--enable-bootstrap --with-gcj=/usr/bin/gcj"
  # Bootstrap without cacao/jamvm:
  ARCH_OPTS=""
else
  # Use a pre-installed OpenJDK:
  export JVMDIR=/usr/lib${LIBDIRSUFFIX}/java
  EXTRA_OPTS="--disable-bootstrap"
fi

export ANT_HOME=/usr/share/ant
export ALT_FREETYPE_LIB_PATH=/usr/lib${LIBDIRSUFFIX}
export ALT_PARALLEL_COMPILE_JOBS="${NUMJOBS/-j}"
export BUILD_NUMBER=$JAVABLD
export HOTSPOT_BUILD_JOBS="${ALT_PARALLEL_COMPILE_JOBS}"
export LANG=C
export OS_VARIANT_NAME="Slackware"
export OS_VARIANT_VERSION=$(cat /etc/slackware-version | cut -f2 -d' ')

cp $CWD/patches/*.patch $TMP/icedtea-$ICEDTEA/patches/
#export DISTRIBUTION_BOOT_PATCHES="patches/openjdk.arm_fixes.patch"
export DISTRIBUTION_BOOT_PATCHES=""
export DISTRIBUTION_PATCHES="patches/nonreparenting_wm.patch"

# For some reason, it wants this
mkdir -p $JVMDIR

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --with-abs-install-dir=/usr/lib${LIBDIRSUFFIX}/java \
  --with-pkgversion=Slackware \
  --with-parallel-jobs=${HOTSPOT_BUILD_JOBS} \
  --with-corba-src-zip=$CWD/sources/${SRC_CORBA} \
  --with-hotspot-src-zip=$CWD/sources/${SRC_HOTSPOT} \
  --with-jaxp-src-zip=$CWD/sources/${SRC_JAXP} \
  --with-jaxws-src-zip=$CWD/sources/${SRC_JAXWS} \
  --with-jdk-src-zip=$CWD/sources/${SRC_JDK} \
  --with-langtools-src-zip=$CWD/sources/${SRC_LANGTOOLS} \
  --with-openjdk-src-zip=$CWD/sources/${SRC_OPENJDK} \
  --with-jdk-home=${JVMDIR} \
  --without-rhino \
  --disable-docs \
  --disable-jdk-tests \
  --disable-langtools-tests \
  --disable-system-gif \
  --disable-system-jpeg \
  --disable-system-kerberos \
  --disable-system-lcms \
  --disable-system-pcsc \
  --disable-system-png \
  --disable-system-sctp \
  --disable-system-zlib \
  --enable-nss \
  $ARCH_OPTS \
  $EXTRA_OPTS \
  --build=$CONFIGURE_TRIPLET || exit 1
  #--with-javac=${JVMDIR}/bin/javac \

# In bootstrap build on Slackware 14, avoid a DSO linking error:
sed -i -e "s/-o native-ecj/-lgcj -o native-ecj/" Makefile

make VERBOSE=TRUE || exit 1

# Move the generated jre and sdk images to the respective packages:
mkdir -p $PKG1/usr/lib${LIBDIRSUFFIX} $PKG2/usr/lib${LIBDIRSUFFIX}/java
cp -a openjdk.build/j2sdk-image \
  $PKG1/usr/lib${LIBDIRSUFFIX}/java
cp -a openjdk.build/j2re-image \
  $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre

# Fix a file permission issue,
# see also http://icedtea.classpath.org/bugzilla/show_bug.cgi?id=1437
chmod 644 $PKG1/usr/lib${LIBDIRSUFFIX}/java/lib/sa-jdi.jar

if [ "$DEBUGBUILD" = "NO" ]; then
  # Strip debug symbols from the libjvm.so but leave the symbol tables alone:
  find $PKG1 -name libjvm.so -exec strip --strip-debug {} \;
  find $PKG2 -name libjvm.so -exec strip --strip-debug {} \;
fi

# Generate a cacerts file from the certificates installed through our
# ca-certificates package - the "cacerts" in the OpenJDK sources is empty.
# This will only work if we already have a proper JDK/JRE installed:
if which keytool 1>/dev/null 2>&1 ; then
  mkdir certgen
  ( cd certgen
    cp $CWD/scripts/generate-cacerts.pl .
    for cert in /usr/share/ca-certificates/*/*.crt; do
      openssl x509 -text -in "${cert}" >> all.crt
    done
    perl generate-cacerts.pl keytool all.crt
    install -p -m0644 cacerts $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security
    install -p -m0644 cacerts $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security
  )
else
  echo "** Not installing a proper cacerts file !"
  echo "** Missing keytool program (do you really have a Java installed?)"
fi

# Pick a selection of fontconfig files:
cp -a $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/fontconfig{.Ubuntu,}.properties.src
cp -a $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/fontconfig{.Ubuntu,}.bfc
cp -a $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/fontconfig{.Ubuntu,}.properties.src
cp -a $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/fontconfig{.Ubuntu,}.bfc

# Symlinks are present in Slackware historically:
( cd $PKG1/usr/lib${LIBDIRSUFFIX}
  ln -sf java jdk${VERSION}
) || exit 1
( cd $PKG2/usr/lib${LIBDIRSUFFIX}/java
  ln -sf jre jre${VERSION}
) || exit 1

# Add the profile scripts:
mkdir -p $PKG1/etc/profile.d $PKG2/etc/profile.d
for file in $(ls $CWD/profile.d/openjdk*) ; do
  cat $file | sed -e "s#lib/java#lib${LIBDIRSUFFIX}/java#" \
    > $PKG1/etc/profile.d/$(basename $file)
done
for file in $(ls $CWD/profile.d/openjre*) ; do
  cat $file | sed -e "s#lib/java#lib${LIBDIRSUFFIX}/java#" \
    > $PKG2/etc/profile.d/$(basename $file)
done
chmod 755 $PKG1/etc/profile.d/* $PKG2/etc/profile.d/*

# Add menu entries and icons:
for i in 16 24 32 48 ; do
  mkdir -p ${PKG1}/usr/share/icons/hicolor/${i}x${i}/apps
  install -m644 openjdk/jdk/src/solaris/classes/sun/awt/X11/java-icon${i}.png \
  ${PKG1}/usr/share/icons/hicolor/${i}x${i}/apps/java.png 
  mkdir -p ${PKG2}/usr/share/icons/hicolor/${i}x${i}/apps
  install -m644 openjdk/jdk/src/solaris/classes/sun/awt/X11/java-icon${i}.png \
  ${PKG2}/usr/share/icons/hicolor/${i}x${i}/apps/java.png 
done
mkdir -p ${PKG1}/usr/share/applications
install -m644 *.desktop ${PKG1}/usr/share/applications/
mkdir -p ${PKG2}/usr/share/applications
install -m644 *.desktop ${PKG2}/usr/share/applications/
# We do not create symlinks for binaries in /usr/bin , instead we add the
# Java directory to the $PATH :
for FILE in ${PKG1}/usr/share/applications/*.desktop \
 ${PKG2}/usr/share/applications/*.desktop ; do
  sed -i -e "s,^Exec=/usr/bin/,Exec=," $FILE
done

# User-modifiable configuration files go to /etc :
mkdir -p $PKG1/etc/java $PKG2/etc/java
mv $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/${LIB_ARCH}/jvm.cfg $PKG1/etc/java/jvm.cfg.new
ln -sf /etc/java/jvm.cfg $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/${LIB_ARCH}/jvm.cfg
mv $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.policy $PKG1/etc/java/java.policy.new
ln -sf /etc/java/java.policy $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.policy
mv $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.security $PKG1/etc/java/java.security.new
ln -sf /etc/java/java.security $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.security
cat $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/nss.cfg | sed -e 's/seamonkey-.*$/seamonkey/' > $PKG1/etc/java/nss.cfg.new
ln -sf /etc/java/nss.cfg $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/nss.cfg
# And the jre package:
mv $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/${LIB_ARCH}/jvm.cfg $PKG2/etc/java/jvm.cfg.new
ln -sf /etc/java/jvm.cfg $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/${LIB_ARCH}/jvm.cfg
mv $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.policy $PKG2/etc/java/java.policy.new
ln -sf /etc/java/java.policy $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.policy
mv $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.security $PKG2/etc/java/java.security.new
ln -sf /etc/java/java.security $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/java.security
# Add jdk's nss.cfg file to the jre package (some applications crash otherwise):
cat $PKG1/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/nss.cfg | sed -e 's/seamonkey-.*$/seamonkey/' > $PKG2/etc/java/nss.cfg.new
ln -sf /etc/java/nss.cfg $PKG2/usr/lib${LIBDIRSUFFIX}/java/jre/lib/security/nss.cfg

# NOTE:
# The openjdk does not build a mozilla plugin: install icedtea-web instead.

( cd $PKG1/usr/lib${LIBDIRSUFFIX}
  ln -sf ./java/jre/lib/${LIB_ARCH}/server/libjvm.so .
)

( cd $PKG2/usr/lib${LIBDIRSUFFIX}
  ln -sf ./java/jre/lib/${LIB_ARCH}/server/libjvm.so .
)

# Add package description and deal with the .new files:
mkdir -p $PKG1/install $PKG2/install
cat $CWD/slack-desc.jdk > $PKG1/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG1/install/doinst.sh
cat $CWD/slack-desc.jre > $PKG2/install/slack-desc
zcat $CWD/doinst.sh.gz  > $PKG2/install/doinst.sh

# Add documentation files:
mkdir -p $PKG1/usr/doc/$NAME1-$VERSION/icedtea
mkdir -p $PKG2/usr/doc/$NAME2-$VERSION/icedtea
( cd openjdk
  cp -a \
    ASSEMBLY_EXCEPTION LICENSE README* THIRD_PARTY_README \
    $PKG1/usr/doc/$NAME1-$VERSION
  cp -a \
    ASSEMBLY_EXCEPTION LICENSE README* THIRD_PARTY_README \
    $PKG2/usr/doc/$NAME2-$VERSION
)
cp -a \
  AUTHORS COPYING HACKING INSTALL NEWS README* THANKYOU \
  $PKG1/usr/doc/$NAME1-$VERSION/icedtea
cp -a \
  AUTHORS COPYING HACKING INSTALL NEWS README* THANKYOU \
  $PKG2/usr/doc/$NAME2-$VERSION/icedtea

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  for DOCSDIR in $(echo $PKG1/usr/doc/${NAME1}-$VERSION) \
    $(echo $PKG2/usr/doc/${NAME2}-$VERSION)
  do
    cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
    touch -r ChangeLog $DOCSDIR/ChangeLog
  done
fi

# Finally, create the two packages:
cd $PKG1
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR/slack-required.$NAME1 $PKG1
echo "Creating package $NAME1-$VERSION-$ARCH-$BUILD.txz"
/sbin/makepkg -l y -c n $RELEASEDIR/$NAME1-$(echo $VERSION | tr - _)-$ARCH-$BUILD.txz
cd $PKG2
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR/slack-required.$NAME2 $PKG2
echo "Creating package $NAME2-$VERSION-$ARCH-$BUILD.txz"
/sbin/makepkg -l y -c n $RELEASEDIR/$NAME2-$(echo $VERSION | tr - _)-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
fi
