# You can run this script in the sources/ subdirectory to download the sources.

# -----------------------------------------------------------------------------
#
# These source URLs are constructed from reading the Makefile.in of icedtea:
# egrep '(^OPENJDK_VERSION|^JDK_UPDATE_VERSION|^BUILD_VERSION|^CORBA_CHANGESET|^HOTSPOT_CHANGESET|^JAXP_CHANGESET|^JAXWS_CHANGESET|^JDK_CHANGESET|^LANGTOOLS_CHANGESET|^OPENJDK_CHANGESET|^CACAO_VERSION|^CACAO_BASE_URL|^JAMVM_VERSION|^JAMVM_BASE_URL|^ICEDTEA_MAJOR|^ICEDTEA_BRANCH|^ICEDTEA_PREFIX|^ICEDTEA_HG_URL|^DROP_URL)' Makefile.in
#
# In icedtea you can choose between "default" and "zero" versions
# of the VM. The zero vm is meant for ARM based systems.
# Use this command to find out the hash of the hopspot.zip
# grep default hotspot.map
#
# -----------------------------------------------------------------------------

# Check ICEDTEA_BRANCH value in Makefile.in:
ICEDTEA_MAJOR="icedtea7"
ICEDTEA_BRANCH="2.6.1"
ICEDTEA_VERSION="2.6.1"
DROPURL="http://icedtea.classpath.org/download/drops"
SUPPURL="${DROPURL}/${ICEDTEA_MAJOR}/${ICEDTEA_BRANCH}"

# Get icedtea:
wget -c http://icedtea.classpath.org/download/source/icedtea-${ICEDTEA_VERSION}.tar.xz
wget -c http://icedtea.classpath.org/download/source/icedtea-${ICEDTEA_VERSION}.tar.xz.sig

# Get supporting sources:
wget ${SUPPURL}/openjdk.tar.bz2 -O openjdk_icedtea-${ICEDTEA_VERSION}.tar.bz2
wget ${SUPPURL}/corba.tar.bz2 -O corba_icedtea-${ICEDTEA_VERSION}.tar.bz2 
wget ${SUPPURL}/jaxp.tar.bz2 -O jaxp_icedtea-${ICEDTEA_VERSION}.tar.bz2 
wget ${SUPPURL}/jdk.tar.bz2 -O jdk_icedtea-${ICEDTEA_VERSION}.tar.bz2 
wget ${SUPPURL}/langtools.tar.bz2 -O langtools_icedtea-${ICEDTEA_VERSION}.tar.bz2 
wget ${SUPPURL}/jaxws.tar.bz2 -O jaxws_icedtea-${ICEDTEA_VERSION}.tar.bz2 
wget ${SUPPURL}/hotspot.tar.bz2 -O hotspot_icedtea-${ICEDTEA_VERSION}.tar.bz2 

# Get these hash values from Makefile.in:
tar xvf icedtea-${ICEDTEA_VERSION}.tar.xz icedtea-${ICEDTEA_VERSION}/Makefile.in
mv icedtea-${ICEDTEA_VERSION}/Makefile.in .
rmdir icedtea-${ICEDTEA_VERSION}
egrep '^CACAO_VERSION|^JAMVM_VERSION' Makefile.in 
CACAO_HASH=$(grep '^CACAO_VERSION' Makefile.in |tr -d ' ' |cut -d= -f2) 
JAMVM_HASH=$(grep '^JAMVM_VERSION' Makefile.in |tr -d ' ' |cut -d= -f2) 
wget ${DROPURL}/cacao/${CACAO_HASH}.tar.xz -O cacao_icedtea-${ICEDTEA_VERSION}.tar.xz
wget ${DROPURL}/jamvm/jamvm-${JAMVM_HASH}.tar.gz -O jamvm_icedtea-${ICEDTEA_VERSION}.tar.gz

echo ==========================================================================
JDKU=$(grep ^JDK_UPDATE_VERSION Makefile.in | tr -d ' ' | cut -f2 -d=)
JDKB=$(grep ^BUILD_VERSION Makefile.in | tr -d ' ' | cut -f2 -d=)
echo "** Icedtea ${ICEDTEA_VERSION} will build OpenJDK update $JDKU build $JDKB"
echo ==========================================================================

