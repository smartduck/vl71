# Create couch user and group
if ! grep -q ^couchdb: etc/group 2>/dev/null ; then
  /usr/sbin/groupadd -g 231 couchdb
elif ! grep -q ^couchdb: etc/passwd 2>/dev/null ; then
  /usr/sbin/useradd -u 231 -g couchdb -d /var/lib/couchdb -s /bin/sh couchdb
fi

config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to
  # consider...
}
                      
preserve_perms() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  if [ -e $OLD ]; then
    cp -a $OLD ${NEW}.incoming
    cat $NEW > ${NEW}.incoming
    mv ${NEW}.incoming $NEW
  fi
  config $NEW
}
                      
preserve_perms etc/rc.d/rc.couchdb.new
config etc/couchdb/default.ini.new
config etc/couchdb/local.ini.new
config etc/logrotate.d/couchdb.new
