#!/bin/sh

# Copyright 2008, 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Note that the slack-desc file mentions the JRE version (edit when upgrading)!
NAME=jre
MAJOR=7
VERSION=7u51
JRE_BUILD=b13
DVER=1.7.0_51

#CFLAGS SETUP
#--------------------------------------------
ARCH=${ARCH:-"$(uname -m)"}
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i486-slackware-linux"
  LIBDIRSUFFIX=""
  JAVA_ARCH="i586"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-slackware-linux"
  LIBDIRSUFFIX="64"
  JAVA_ARCH="x64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://ftp.osuosl.org/pub/funtoo/distfiles/oracle-java/jre-${VERSION}-linux-${JAVA_ARCH}.tar.gz"}

# As of jre-6u12 a 64bit plugin is available.
# SUN says:
# Please note that the 64-bit JRE only works with the 64-bit browser plug-in,
# while the 32-bit JRE only works with 32-bit browser plug-in.
# If you use both 32-bit and 64-bit browsers interchangeably, you will need to
# have both 32-bit and 64-bit JRE's installed on your machine. 
#JAVA_ARCH=${JAVA_ARCH:-i586}      # or i586 if you want the 32bit version

if [ "$JAVA_ARCH" = "x64" ]; then
  LIB_ARCH=amd64
  ARCH=x86_64
  LIBDIRSUFFIX="64"
else
  LIB_ARCH=i386
  ARCH=i586
  LIBDIRSUFFIX=''
fi

#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	# download jre with cookie
	# http://ivan-site.com/2012/05/download-oracle-java-jre-jdk-using-a-script/
	wget --no-cookies --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F" \
	--no-check-certificate -O $NAME-$VERSION-linux-$JAVA_ARCH.tar.gz -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-jre
rm -rf $PKG
mkdir -p $TMP $PKG

cd $PKG
mkdir -p usr/lib${LIBDIRSUFFIX}
cd usr/lib${LIBDIRSUFFIX}
tar xf $CWD/jre-${VERSION}-linux-${JAVA_ARCH}*.tar.gz
mkdir -p $PKG/etc/profile.d
for file in $(ls $CWD/profile.d/*) ; do
  cat $file | sed -e "s#lib/java#lib${LIBDIRSUFFIX}/java#" \
    > $PKG/etc/profile.d/$(basename $file)
done
chown -R root:root $PKG
( cd $PKG
  find . \
   \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
   -exec chmod 755 {} \; -o \
   \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
   -exec chmod 644 {} \;
)
chmod 755 $PKG/etc/profile.d/*
( cd $PKG/usr/lib${LIBDIRSUFFIX}
 # mv jre${DVER} java || exit 1
  ln -sf jre${DVER} java
) || exit 1
mkdir -p $PKG/usr/lib${LIBDIRSUFFIX}/mozilla/plugins
( cd $PKG/usr/lib${LIBDIRSUFFIX}/mozilla/plugins
  ln -sf /usr/lib${LIBDIRSUFFIX}/java/lib/${LIB_ARCH}/libnpjp2.so libnpjp2.so
)
( cd $PKG/usr/lib${LIBDIRSUFFIX}
  ln -sf ./java/lib/${LIB_ARCH}/server/libjvm.so .
)

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cat << EOF > $PKG/install/doinst.sh
# Clean up a potential upgrade mess caused by changing the
# installation directory to /usr/lib${LIBDIRSUFFIX}/java/:
if [ -L usr/lib${LIBDIRSUFFIX}/java ]; then
  rm -rf usr/lib${LIBDIRSUFFIX}/java
  mkdir -p usr/lib${LIBDIRSUFFIX}/java/man
  mkdir -p usr/lib${LIBDIRSUFFIX}/java/lib/${LIB_ARCH}/server
  mkdir -p usr/lib/java/lib${LIBDIRSUFFIX}/${LIB_ARCH}/client
  mkdir -p usr/lib${LIBDIRSUFFIX}/java/javaws
  mkdir -p usr/lib${LIBDIRSUFFIX}/java/bin
  mkdir -p usr/lib${LIBDIRSUFFIX}/mozilla/plugins
fi
if [ -L ControlPanel ]; then
  rm -f ControlPanel
fi
if [ -L ja ]; then
  rm -f ja
fi
if [ -L javaws ]; then
  rm -f javaws
fi
if [ -L libjsig.so ]; then
  rm -f libjsig.so
fi
# End cleanup.
EOF

# Install the desktop/mime files:
mkdir -p $PKG/usr/share/{applications,icons,mime}
cp -a $PKG/usr/lib${LIBDIRSUFFIX}/java/lib/desktop/applications/* \
  $PKG/usr/share/applications/
cp -a $PKG/usr/lib${LIBDIRSUFFIX}/java/lib/desktop/icons/hicolor \
  $PKG/usr/share/icons/
cp -a $PKG/usr/lib${LIBDIRSUFFIX}/java/lib/desktop/mime/packages \
  $PKG/usr/share/mime/

# We ship the package unmodified, so we do not compress manpages.

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP


